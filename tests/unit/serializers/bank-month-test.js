import { moduleForModel, test } from 'ember-qunit';

moduleForModel('bank-month', 'Unit | Serializer | bank month', {
  // Specify the other units that are required for this test.
  needs: ['serializer:bank-month']
});

// Replace this with your real tests.
test('it serializes records', function(assert) {
  let record = this.subject();

  let serializedRecord = record.serialize();

  assert.ok(serializedRecord);
});
