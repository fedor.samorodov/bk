import Ember from 'ember';
import config from './config/environment';

const Router = Ember.Router.extend({
  location: config.locationType
});

Router.map(function() {
  this.route('reports', function() {
    this.route('view', { path: "/:report_id" });
  });
  this.route('login');
  this.route('bank', function() {
    this.route('processing');
    this.route('lists', { path: "lists/:month_id" });
    this.route('uploads');
    this.route('month', { path: "month/:month_id" }, function() {
      this.route('year', { path: "/:year_id" });
    });
  });
  this.route('500');
  this.route('404', { path: '/*path' });
});

export default Router;
