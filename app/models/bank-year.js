import DS from 'ember-data';

var attr = DS.attr,
    hasMany = DS.hasMany;

export default DS.Model.extend({
  date: attr('string'),
  comming: attr('string'),
  expense: attr('string'),
  acquiring: attr('string'),
  month: hasMany('bank-month', {async: true})
});


