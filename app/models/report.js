import DS from 'ember-data';

var attr = DS.attr,
    hasMany = DS.hasMany;

export default DS.Model.extend({
  date: attr('string'),
  expenses: hasMany('expense', {async: true}),
  status: attr('string'),
  count_receipts: attr('number'),
  cash_acquiring: attr('string'),
  cash_revenues: attr('string'),
  sent_at: attr('string'),
  formattedDate: function() {
    var date = this.get('date');
    return moment(date).format('LL');
  }.property('date'),
  status_title: function() {
    if (this.get('status') == 1) {
      return 'Не отправлен'
    } else if (this.get('status') == 2) {
      return 'Отправлен'
    } else if (this.get('status') == 3) {
      return 'Не отправлен и просрочен'
    } else {
      return 'Отправлен с просрочкой'
    }
  }.property('status'),
  status_class: function() {
    if (this.get('status') == 1) {
      return 'default'
    } else if (this.get('status') == 2) {
      return 'success'
    } else if (this.get('status') == 3) {
      return 'danger'
    } else {
      return 'warning'
    }
  }.property('status')
});


