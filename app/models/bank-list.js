import DS from 'ember-data';

var attr = DS.attr;

export default DS.Model.extend({
  date: attr('string'),
  contractor: attr('string'),
  comming: attr('string'),
  unit: DS.belongsTo('expense-category', {async: true}),
  expense: attr('string'),
  balance: attr('string')
});


