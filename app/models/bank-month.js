import DS from 'ember-data';

var attr = DS.attr,
    hasMany = DS.hasMany;

export default DS.Model.extend({
  date: attr('string'),
  balance: attr('string'),
  b_date: attr('string'),
  comming: attr('string'),
  expense: attr('string'),
  acquiring: attr('string'),
  transactions: hasMany('bank-list', {async: true}),
  statistics: hasMany('bank-stat', {async: true}),
  transact_num: function() {
    return this.get('transactions').get('length');
  }.property('transactions')
});


