import DS from 'ember-data';

export default DS.Model.extend({
  sum: DS.attr('string'),
  category: DS.belongsTo('expense-category', {async: true}),
  comment: DS.attr('string'),
  report: DS.belongsTo('report', {async: true})
});
