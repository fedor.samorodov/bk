import DS from 'ember-data';

var attr = DS.attr,
    hasMany = DS.hasMany;

export default DS.Model.extend({
  title: DS.belongsTo('expense-category', {async: true}),
  dinamic_stat: attr('string'),
  dinamic: attr('string'),
  summ: attr('string'),
  transactions: hasMany('bank-list', {async: true}),
  num: function() {
    return this.get('transactions').get('length');
  }.property('transactions')
});


