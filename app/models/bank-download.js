import DS from 'ember-data';

var attr = DS.attr;

export default DS.Model.extend({
  date: attr('string'),
  name: attr('string'),
  link: attr('string'),
  transactions: DS.hasMany('bank-list', {async: true}),
  all: function() {
    return this.get('transactions').get('length');
  }.property('transactions'),
  new: function() {
    return this.get('transactions').getWithDefault('balance', '2');
  }.property('transactions')
});

