import Ember from 'ember';
import AuthenticatedRouteMixin from 'ember-simple-auth/mixins/authenticated-route-mixin';

export default Ember.Route.extend(AuthenticatedRouteMixin, {
    actions: {   
        willTransition: function(transition) {
            var model = this.currentModel;
            if (model.get('hasDirtyAttributes')) {
                if (window.confirm("Имеются несохраненные данные, вы действительно хотите выйти?")) {
                    model.rollbackAttributes();
                } else {
                    transition.abort();
                }
            } 
        }
    },    
    model: function  (params) {
        return this.store.findRecord('report', params.report_id)
    },

    setupController: function(controller, model) {
        controller.set('model', model);
        this.store.findAll('expense-category').then(function(units) {
          controller.set('units', units);
        });
    }
});
