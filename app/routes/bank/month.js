import Ember from 'ember';

export default Ember.Route.extend({
	model: function  (params) { 
		return this.store.findRecord('bank-month', params.month_id)
	},

	setupController: function(controller, model) {
		controller.set('model', model)
		this.store.findAll('bank-year').then(function(years) {
			controller.set('currentYear', years.get('firstObject'));
		})
	 }
});
