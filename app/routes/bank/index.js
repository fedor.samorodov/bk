import Ember from 'ember';

export default Ember.Route.extend({
	
	model: function() { 
		return this.store.findAll('bank-month')
	},

	setupController: function(controller, model) {
	    controller.set('month', model.get('firstObject'));
	    controller.transitionToRoute('bank.month', model.get('firstObject'));
	}
});
