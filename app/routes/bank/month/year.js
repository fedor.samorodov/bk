import Ember from 'ember';

export default Ember.Route.extend({

	model: function  (params) { 
		return this.store.findRecord('bank-year', params.year_id)
	},

	setupController: function(controller, model) {
		this.store.findAll('bank-year').then(function(years) {
			controller.set('years', years);
		})
	    controller.set('currentYear', model);
	 }
});
