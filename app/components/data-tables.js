import Ember from 'ember';

export default Ember.Component.extend({
	didInsertElement: function() {
		var data = this.get('model').getEach('data');
		console.log(data)
	    $('#table').dataTable({
	        "bProcessing": true,
	        "aaData": data,
	        "aoColumns": [
	          { "sTitle" : "Дата", "mData": "date" },
	          { "sTitle" : "Контрагент", "mData": "contractor" },
	          { "sTitle" : "Приход", "mData": "comming" },
	          { "sTitle" : "Расход", "mData": "expense" },
	          { "sTitle" : "Баланс", "mData": "balance" }
	        ]
	      });
	    $('#table .table-caption').text('Список транзакций за декабрь 2015');   
		
	}
});
