import Ember from 'ember';

import DataAdapterMixin from 'ember-simple-auth/mixins/data-adapter-mixin';
import SessionService from 'ember-simple-auth/services/session';
export default DS.JSONAPIAdapter.extend( {
	session: Ember.inject.service('session'),
	headers: Ember.computed('session', function() {
		var token = this.get("session.data.authenticated.access_token");
	    return {
	      "access-token": token,
	      "Content-Type": "application/vnd.api+json"
	    };
	  }),	
 	host: 'http://192.81.221.206',
 	namespace: 'api'
});