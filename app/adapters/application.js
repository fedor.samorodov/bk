import Ember from 'ember';

import DataAdapterMixin from 'ember-simple-auth/mixins/data-adapter-mixin';

export default DS.JSONAPIAdapter.extend(DataAdapterMixin, {
    headers: Ember.computed('session', function() {
	    return {
	      "access-token": this.get("session.data.authenticated.access_token")
	    };
	  }),
 	authorizer: 'authorizer:oauth2',
 	host: 'http://192.81.221.206',
 	namespace: 'api'
});



