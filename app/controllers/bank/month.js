import Ember from 'ember';

export default Ember.Controller.extend({
	actions: {
		getTransactions: function  (trans) {
			this.set('currentTrans', trans)
		}
	}
});
