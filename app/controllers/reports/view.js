import Ember from 'ember';
import EmberValidations from 'ember-validations';
export default Ember.Controller.extend( EmberValidations, {
    session: Ember.inject.service('session'),
    validations: {
        sum: {presence: {message: 'Введите сумму'}},
        unit: {presence: {message: 'Выберите статью'}},
        comment: {presence: {message: 'Введите комментарий'}}
    },

    actions: {

        //добавление статьи
        addList: function() {
            var _this = this;
            var report = _this.get('model');
            _this.validate().then(function() {
                var list = _this.store.createRecord('expense', {
                    sum: _this.get('sum'),
                    comment: _this.get('comment'),
                    category: _this.get('unit'),
                    report: report
                });
                report.get('expenses').addObject(list);
                list.save();

                _this.set('sum', '');
                _this.set('comment', '');
                _this.set('err', '');
            }).catch(function() {
                var err = _this.get('errors');
                _this.set('err', err);
            });
        },

        //сохрание отчета
        save: function() {
            const flashMessages = Ember.get(this, 'flashMessages');

            this.get('model').save().then(function() {
                flashMessages.success('Изменения сохранены!');
            }).catch(function(err) {
                flashMessages.danger('Что-то пошло не так:(');
                console.log(err);
            });

            console.log(this.get('session.data.authenticated.access_token'))

        },
        //отправка отчета
        send: function() {
            var _this = this;
            var report = this.get('model');
            report.set('status', 2);
            report.save().then(function() {
                _this.transitionToRoute('reports');
            });
        },
        ///удаление
        delete: function(list) {
            if (window.confirm("Элемент будет удален безвозвратно, продолжить?")) {
                var report = this.get('model');
                list.destroyRecord().then(function() {
                    report.save();
                });
            }
        },

        //добавление статьи
        addCat: function() {
            var _this = this;
            var name = _this.get('newCat')
            if(name) {
                const flashMessages = Ember.get(this, 'flashMessages');
                var cat = _this.store.createRecord('expense-category', {
                        name: name
                    });
                cat.save().then(function() {
                        flashMessages.success('Статья добавлена!');
                        $('.modal').modal('hide');
                    }).catch(function(err) {
                        flashMessages.danger('Что-то пошло не так:(');
                        console.log(err);
                    });
                _this.set('newCat', '');
            }
            
        }
    }
});